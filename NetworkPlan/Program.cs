﻿using NetworkPlan.FileHelper;
using System;
using System.Collections.Generic;
using System.IO;

namespace NetworkPlan
{
    class Program
    {
        static void Main(string[] args)
        {

#if DEBUG
            Console.WriteLine("Mode=Debug");
#else
            Console.WriteLine("Mode=Release");
            if(args.Length > 0)
            {

                if (!File.Exists(
                    System.IO.Path.Combine(
                        Directory.GetCurrentDirectory(), 
                        args[0])))
                {
                    Console.WriteLine("Die spezifizierte Datei ist nicht gültig!");
                    return;
                }
            }
            else
            {
                Console.WriteLine("Es wurde keine Datei angegeben");
                return;
            }

#endif

            string file = "";
#if DEBUG
            //file = @"..\..\..\TestData\Input\MyTests\8_Beispiel_FehlerhafteEingabe.txt";
            //file = @"..\..\..\TestData\Input\MyTests\7_Beispiel_AusInternet.txt";
            file = @"..\..\..\TestData\Input\A.txt";
#else
            file = args[0];
#endif

            Console.WriteLine("Netzplan Input Datei wird eingelesen und analysiert!");
            List<Node> startNodes = 
                InputReader.Analyze(InputReader.ReadFile(file));

            OutputWriter.PrintErrors(InputReader.Errors);
            if (InputReader.FileContainsErrors())
            {
                Console.ReadKey();
                return;
            }

            NetworkPlan networkPlan 
                = new NetworkPlan(startNodes, InputReader.AllNodes);

            Console.WriteLine("Die Kritischen Pfade werden gesucht");
            networkPlan.FindCriticalPaths();

            Console.Write("Die Outputdatei wird geschrieben");
            OutputWriter.SetHeaderComment(InputReader.HeaderComment);
            Console.WriteLine("\n");

            OutputWriter.Write(networkPlan);
            Console.ReadKey();

            //TODO: Code comments
        }
    }
}
