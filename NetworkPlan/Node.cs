﻿using System.Collections.Generic;
using System.Linq;

namespace NetworkPlan
{
    public class Node
    {
        #region Member Variables

        /// <summary>
        /// A list of parent nodes that the node has
        /// </summary>
        public List<Node> Parents
        { get; private set; }

        /// <summary>
        /// A list of child nodes that the nodehas
        /// </summary>
        public List<Node> Children
        { get; private set; }

        /// <summary>
        /// Description, or title, of the node
        /// </summary>
        public string Description
        { get; private set; }

        /// <summary>
        /// The amount of time it will take to complete this node
        /// </summary>
        public double Duration
        { get; private set; }

        /// <summary>
        /// The ID of the node
        /// </summary>
        public int ID
        { get; private set; }

        /// <summary>
        /// The earliest beginning time of the node
        /// </summary>
        public int FAZ
        { get; set; }

        /// <summary>
        /// The earliest ending time of the node
        /// </summary>
        public int FEZ
        { get; set; }

        /// <summary>
        /// The latest starting time of the node
        /// </summary>
        public int SAZ
        { get; set; }

        /// <summary>
        /// The latest ending time of the node
        /// </summary>
        public int SEZ
        { get; set; }

        public int GP
        { get; set; }
        public int FP
        { get; set; }

        /// <summary>
        /// The determination of whether or not this node is critical.
        /// A node is critical if the GP and FP are both equal to 0.
        /// This means that, if the completion of the node is delayed, the entire completion
        /// of the network plan is delayed
        /// </summary>
        public bool IsCritical
        { get; set; }
        #endregion

        public Node(
            int id,
            string desc,
            double duration,
            List<Node> parents,
            List<Node> children)
        {
            ID = id;
            Description = desc;
            Duration = duration;
            Parents = parents;
            Children = children;
            IsCritical = false;
        }

        #region Public Member Methods

        /// <summary>
        /// Calculates the FAZ, FEZ of the node.
        /// This method needs to be called before BackPropagate() since the information
        /// in BackPropagate() can only be calculated when the Forward Propagation has completed
        /// </summary>
        public void ForwardPropagate()
        {
            //Set the FAZ to the maximum FEZ of the parents
            FAZ = GetMaxFEZFromParent();
            FEZ = FAZ + (int)Duration;
            if (Children.Count != 0)
            {
                //Forward propagated through each child to 
                //calculate the FAZ and FEZ of the children
                foreach (Node node in Children)
                {
                    node.ForwardPropagate();
                }
            }

            //When an end node has been found, 
            //start the back propagation to calculate the SEZ and SAZ
            if (Children.Count == 0)
            {
                BackPropagate();
            }
        }

        /// <summary>
        /// Calculates the SEZ and SAZ for node
        /// </summary>
        public void BackPropagate()
        {
            if (Children.Count != 0) //If the node contains children, get the minimum SEZ from the children
            {
                SEZ = GetMinSAZFromChildren();
            }
            else //If the current node is an end node
            {
                SEZ = FEZ;
            }

            SAZ = SEZ - (int)Duration;

            //BackPropagate through each parent in order to calculate the necessary values
            foreach (Node node in Parents)
            {
                node.BackPropagate();
            }
        }

        /// <summary>
        /// Determines the Free Buffer(FP) and Total Buffer(GP) of the node
        /// </summary>
        public void DetermineBuffer()
        {
            GP = SEZ - FEZ;
            FP = 0; //If the node is an end node, set the free buffer to 0 since the buffer will have been used up
            if (Children.Count != 0) //Otherwise, calculate the FP by subtracting the FEZ from the current node from the minimal FAZ from the children
            {
                FP = GetMinFAZFromChildren() - FEZ;
            }

            //A node is critical if the FP and GP are both equal to 0
            if (GP == 0 && FP == 0)
            {
                IsCritical = true;
            }

            //Determine the buffer of each children of the current node
            foreach (Node node in Children)
            {
                node.DetermineBuffer();
            }
        }

        public List<Path> FindCriticalPath(Node previousNode)
        {
            List<Path> paths = new List<Path>();
            if (!previousNode.IsCritical)
            {
                return paths;
            }

            foreach (Node node in Children
                .Where(child => child.IsCritical))
            {
                Path path = new Path();
                path.PathNodes.Add(previousNode);
                if (node.Children.Count == 0) //End
                {
                    path.PathNodes.Add(node);
                    paths.Add(path);
                    return paths;
                }
                else //element has children
                {
                    paths.AddRange(
                        node.FindCriticalPath(node));
                }
            }

            foreach (Path criticalPath in paths)
            {
                criticalPath.PathNodes.
                    Insert(0, previousNode);
            }

            if (paths.Count == 0 
                && previousNode.IsCritical)
            {
                Path path = new Path();
                path.PathNodes.Add(previousNode);
                paths.Add(path);
            }

            return paths;
        }

        /// <summary>
        /// This method finds all of the parent nodes recursively, starting at a given node
        /// </summary>
        /// <param name="node">The node for which the parents should be found</param>
        /// <returns>Returns a list of parent nodes</returns>
        public static List<Node> GetAllParentNodes(Node node)
        {
            List<Node> parentNodes = new List<Node>();
            foreach (Node parent in node.Parents)
            {
                parentNodes
                    .Add(parent);
                parentNodes
                    .AddRange(GetAllParentNodes(parent));
            }
            return parentNodes;
        }
        #endregion

        #region Private Member Methods
        private int GetMinSAZFromChildren()
        {
            return Children.Min(child => child.SAZ);
        }

        /// <summary>
        /// Finds the minimal FAZ from the current nodes children
        /// If the node contains no children, return 0
        /// </summary>
        /// <returns>Returns the minimal FAZ from the nodes children</returns>
        private int GetMinFAZFromChildren()
        {
            if (Children.Count == 0)
            {
                return 0;
            }
            return Children.Min(child => child.FAZ);
        }

        /// <summary>
        /// If the node has multiple parents, the maximum FEZ of the parents is returned.
        /// If the node is a starting node, the node has an FAZ of 0
        /// </summary>
        /// <returns>Returns the maximum FEZ of the parents</returns>
        private int GetMaxFEZFromParent()
        {
            if (Parents.Count == 0)
            {
                return 0;
            }
            return Parents.Max(parent => parent.FEZ);
        }
        #endregion

        public bool IsEndNode()
        {
            return Children.Count == 0;
        }

        public override string ToString()
        {
            return ID + "; " 
                + Description.Trim() + "; " 
                + Duration + "; " 
                + FAZ + "; " 
                + FEZ + "; " 
                + SAZ + "; " 
                + SEZ + "; " 
                + GP + "; " 
                + FP;
        }
    }
}
