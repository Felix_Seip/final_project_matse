﻿using System.Collections.Generic;

namespace NetworkPlan
{
    /// <summary>
    /// The Path class can be used to represent a critical path, a cycle, etc.
    /// The path contains a list of nodes that are called one after another (parents and children)
    /// </summary>
    public class Path
    {
        /// <summary>
        /// List of Nodes that are contained in the path
        /// </summary>
        public List<Node> PathNodes { get; set; }
        public Path()
        {
            PathNodes = new List<Node>();
        }

        /// <returns>Returns the string representation of the path</returns>
        public override string ToString()
        {
            string criticalPath = "";
            int index = 0;
            foreach(Node node in PathNodes)
            {
                if(index == PathNodes.Count - 1)
                {
                    criticalPath += node.ID;
                    return criticalPath;
                }
                criticalPath += node.ID + "->";
                index++;
            }

            return criticalPath;
        }
    }
}