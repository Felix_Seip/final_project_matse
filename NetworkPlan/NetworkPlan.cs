﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NetworkPlan
{
    public class NetworkPlan
    {
        /// <summary>
        /// A list of start nodes that are contained in the NetworkPlan
        /// </summary>
        public List<Node> StartNodes
        { get; set; }
        
        /// <summary>
        /// A list of all nodes that are contained in the NetworkPlan
        /// </summary>
        public List<Node> AllNodes
        { get; set; }

        /// <summary>
        /// A list of critical paths
        /// Critical paths are paths that only contain nodes that are marked as critical
        /// Nodes are critical when the FP and GP are both equal to 0
        /// </summary>
        public List<Path> CriticalPaths
        { get; set; }

        /// <summary>
        /// Returns the project duration
        /// This is the maximal SEZ of an end node 
        /// </summary>
        public double ProjectDuration
        {
            get
            {
                double duration = -1;
                int endNodeID = -1;

                if (HasLoops)
                {
                    return duration;
                }

                foreach (Node node in AllNodes
                    .Where(endNode => endNode.Children.Count == 0))
                {
                    if (endNodeID == -1)
                    {
                        endNodeID = node.ID;
                        duration = node.SEZ;
                    }

                    if (node.ID != endNodeID)
                    {
                        return -1;
                    }
                }

                return duration;
            }
        }

        /// <summary>
        /// Returns whether or not a NetworkPlan contains loops or cycles
        /// A NetworkPlan contains loops/cycles when a child node of a node is
        /// also contained in its parent nodes
        /// </summary>
        public bool HasLoops
        {
            get
            {
                bool bContainsLoops = false;
                CyclePaths.Clear();
                foreach (Node node in StartNodes)
                {
                    bContainsLoops = ContainsLoops(node);
                }
                return bContainsLoops;
            }
        }

        /// <summary>
        /// A list of loops/cycles that are contained in the NetworkPlan
        /// </summary>
        public List<Path> CyclePaths { get; private set; }

        public NetworkPlan(
            List<Node> startNodes, 
            List<Node> allNodes)
        {
            StartNodes = startNodes;
            AllNodes = allNodes;
            CriticalPaths = new List<Path>();
            CyclePaths = new List<Path>();

            if (HasLoops) //If the NetworkPlan contains loops/cycles, 
                          //then the calculation fo the NetworkPlan cannot be performed
            {
                return;
            }

            if (IsFullyConnected())
            {
                foreach (Node node in StartNodes)
                {
                    node.ForwardPropagate();
                }

                foreach (Node node in StartNodes)
                {
                    node.DetermineBuffer();
                }
            }
        }

        /// <summary>
        /// Finds the critical paths that are contained in the NetworkPlan
        /// </summary>
        public void FindCriticalPaths()
        {
            foreach (Node node in StartNodes
                .Where(x => x.Parents.Count == 0))
            {
                CriticalPaths.AddRange(
                    node.FindCriticalPath(node));
            }
        }

        /// <summary>
        /// Returns whether or not a NetworkPlan contains loops or cycles
        /// A NetworkPlan contains loops/cycles when a child node of a 
        /// node is also contained in its parent nodes
        /// </summary>
        private bool ContainsLoops(Node currentNode)
        {
            List<Node> parentNodes = Node.GetAllParentNodes(currentNode);
            if (currentNode.Children
                .Any(child => parentNodes
                .Any(parent => child.Equals(parent))) 
                || currentNode.Children.Contains(currentNode))
            {
                //Find all parent nodes that are also a child of the currentNode
                List<Node> loopedNodes = 
                    currentNode.Children.Where(node => parentNodes.Contains(node)).ToList();

                if (loopedNodes.Count == 0)
                {
                    loopedNodes.Add(currentNode);
                }

                //For each parent node that is repeated in the children,
                //determine the path of the loops/cycles
                foreach (Node loopNode in loopedNodes)
                {
                    Path path = new Path();
                    path.PathNodes.Add(loopNode);
                    Node nextNode = loopNode;

                    while (nextNode != currentNode)
                    {
                        foreach (Node child in nextNode.Children)
                        {
                            path.PathNodes.Add(child);
                            nextNode = child;
                        }
                    }

                    path.PathNodes.Add(path.PathNodes[0]);

                    if (!CyclePaths
                        .Any(cyclePath => 
                        String.Compare(
                            cyclePath.ToString(), 
                            path.ToString(), true) == 0))
                    {
                        CyclePaths.Add(path);
                    }
                }
            }
            else
            {
                //If the parent nodes are not repeated in the currentNodes children, 
                //go to each child, and repeat the process
                foreach (Node child in currentNode.Children)
                {
                    ContainsLoops(child);
                }
            }

            //If the amount of loops/cycles that are found is greater than or equal to 1,
            //return true
            return CyclePaths.Count >= 1;
        }

        public bool IsFullyConnected()
        {
            return true;
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.Append("Vorgangsnummer; Vorgangsbezeichung; D; FAZ; FEZ; SAZ; SEZ; GP; FP\r\n");
            foreach (Node node in AllNodes)
            {
                stringBuilder.Append(node.ToString());
                stringBuilder.Append("\r\n");
            }

            stringBuilder.Append("\r\n");
            stringBuilder.Append("Anfangsvorgang: ");

            int index = 0;
            Array.ForEach(StartNodes.ToArray(), startNode =>
            {
                if (index == StartNodes.Count - 1)
                {
                    stringBuilder.Append(startNode.ID);
                }
                else
                {
                    stringBuilder.Append(startNode.ID + ",");
                }
                index++;
            });

            stringBuilder.Append("\r\n");

            index = 0;
            stringBuilder.Append("Endvorgang: ");
            Array.ForEach(AllNodes.Where(endNode => endNode.IsEndNode()).ToArray(), x =>
            {
                if (index == AllNodes.Where(node => node.IsEndNode()).ToList().Count - 1)
                {
                    stringBuilder.Append(x.ID);
                }
                else
                {
                    stringBuilder.Append(x.ID + ",");
                }
                index++;
            });

            stringBuilder.Append("\r\n");
            stringBuilder.Append("Gesamtdauer: " + (ProjectDuration == -1 ? "Nich eindeutig" : "" + ProjectDuration) + "\r\n\r\n");

            stringBuilder.Append(CriticalPaths.Count > 1 ? "Kritische(r) Pfad\r\n" : "Kritischer Pfad\r\n");
            foreach (Path critPath in CriticalPaths)
            {
                stringBuilder.Append(critPath.ToString() + "\r\n");
            }
            return stringBuilder.ToString();
        }
    }
}
