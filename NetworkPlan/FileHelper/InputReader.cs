﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace NetworkPlan.FileHelper
{
    public static class InputReader
    {
        public static List<Node> AllNodes
        { get; private set; }
        public static List<Error> Errors
        { get; private set; }
        public static List<int> NodeIDs
            = new List<int>();

        public static string HeaderComment
        { get; private set; }

        const string COMMENT_CHAR = "/";
        const string HEADER_CHAR = "+";
        const char DATA_SEPARATOR = ';';

        private struct DataInfo
        {
            public string Header;
            public string Data;
            public DataInfo(string header, string data)
            {
                Header = header;
                Data = data;
            }
        }

        private enum ENeighborType
        {
            Parent,
            Child
        }

        public static string[] ReadFile(string fileName)
        {
            HeaderComment = System.IO.Path.GetFileName(fileName);
            return File.ReadAllLines(fileName);
        }

        public static List<Node> Analyze(string[] fileLines)
        {
            if (Errors == null)
            {
                Errors = new List<Error>();
            }

            Errors.Clear();
            string[] objectHeaders = null;

            List<List<DataInfo>> allFileObjects
                = new List<List<DataInfo>>();
            List<Node> nodes = new List<Node>();

            if (fileLines.Length == 0)
            {
                Errors.Add(new Error(
                    "Genereller Datei Fehler",
                    Error.EErrorType.EmptyFile));
                return null;
            }

            foreach (string line in fileLines)
            {
                List<DataInfo> fileObjects = new List<DataInfo>();

                //The header comments that define what each data item is
                ////Vorgangsnummer; Vorgangsbezeichnung; Dauer; Vorgänger; Nachfolger
                if (line.Contains(COMMENT_CHAR) && line.Contains(DATA_SEPARATOR))
                {
                    objectHeaders = line.Split(DATA_SEPARATOR);
                }

                //The comment that defines the name of the example
                ////+ Beispiel 12 - Fehlerhaft Eingabe
                else if (line.Contains(COMMENT_CHAR) && line.Contains(HEADER_CHAR))
                {
                    HeaderComment = line
                        .Replace(COMMENT_CHAR, "")
                        .Replace(HEADER_CHAR, "").Trim();
                    OutputWriter.SetHeaderComment(HeaderComment);
                }

                if (line.Contains(COMMENT_CHAR))
                {
                    continue;
                }

                string[] data = line.Split(DATA_SEPARATOR);
                int index = 0;

                foreach (string dataInfo in data)
                {
                    if(objectHeaders == null)
                    {
                        Errors.Add(new Error("", Error.EErrorType.MissingHeaderComment));
                        return null;
                    }


                    if (objectHeaders[index].Contains("Vorgangsnummer"))
                    {
                        NodeIDs.Add(int.Parse(dataInfo));
                    }

                    fileObjects.Add(
                        new DataInfo
                        {
                            Header = objectHeaders[index].Replace("/", ""),
                            Data = dataInfo
                        });
                    index++;
                }

                bool containsError = false;
                //If the line is missing a semicolon, add an error to the error list
                if (fileObjects.Count < 5)
                {
                    //Add error to list
                    if (!Errors
                        .Any(error =>
                        error.LineInfo.Trim() == line.Trim()))
                    {
                        Errors.Add(
                            new Error(
                                line,
                                Error.EErrorType.MissingSemiColon));
                    }
                    containsError = true;
                }
                else
                {
                    index = 0;
                    foreach (string header in objectHeaders)
                    {
                        //If all five semi colons are found, but a data item is missing
                        if (fileObjects[index].Header != header.Replace("/", "")
                            || fileObjects[index].Data.Trim() == "")
                        {
                            //Add error to list
                            if (!Errors.Any(error => error.LineInfo.Trim() == line.Trim()))
                            {
                                Errors.Add(
                                    new Error(
                                        line,
                                        Error.EErrorType.MissingNodeInfo));
                            }

                            containsError = true;
                        }
                        index++;
                    }

                    if (!containsError)
                    {
                        //If no errors are found, create the node
                        nodes.Add(
                            new Node(
                                int.Parse(fileObjects[0].Data),
                                fileObjects[1].Data,
                                int.Parse(fileObjects[2].Data),
                                new List<Node>(),
                                new List<Node>()));
                    }

                    allFileObjects.Add(fileObjects);
                }
            }

            //Go through each node and add children and parent nodes to the node
            foreach (List<DataInfo> dataInfo in allFileObjects)
            {
                List<DataInfo> parents =
                    dataInfo
                    .Where(info => info.Header.Trim() == objectHeaders[3].Trim()).ToList();
                List<DataInfo> children =
                    dataInfo
                    .Where(info => info.Header.Trim() == objectHeaders[4].Trim()).ToList();
                AddChildrenOrParentsToNodes(
                    parents,
                    ENeighborType.Parent,
                    int.Parse(dataInfo[0].Data),
                    ref nodes);
                AddChildrenOrParentsToNodes(
                    children,
                    ENeighborType.Child,
                    int.Parse(dataInfo[0].Data),
                    ref nodes);
            }

            AllNodes = nodes;
            if (Errors.Count > 0)
            {
                return null;
            }
            return nodes.Where(node => node.Parents.Count == 0).ToList();
        }

        private static void AddChildrenOrParentsToNodes(
            List<DataInfo> dataInfos,
            ENeighborType neighborType,
            int nodeID,
            ref List<Node> nodes)
        {
            foreach (DataInfo info in dataInfos)
            {
                string[] bla = info.Data.Split(',');
                foreach (string neighborInfo in bla)
                {
                    if (neighborInfo.Trim() == "")
                    {
                        //TODO: Add error to list
                        continue;
                    }

                    if (neighborInfo.Trim() != "-")
                    {
                        if (nodes
                            .Where(node => node.ID == int.Parse(neighborInfo.Trim())).Count() > 0)
                        {
                            if (nodes
                                .Where(node => node.ID == nodeID).ToList().Count > 0)
                            {
                                if (neighborType == ENeighborType.Parent)
                                {
                                    nodes
                                        .Where(node => node.ID == nodeID).ToList()[0]
                                        .Parents.Add(nodes
                                        .Where(node => node.ID == int.Parse(neighborInfo.Trim())).ToList()[0]);
                                }
                                else if (neighborType == ENeighborType.Child)
                                {
                                    nodes
                                        .Where(node => node.ID == nodeID).ToList()[0]
                                        .Children.Add(nodes
                                        .Where(node => node.ID == int.Parse(neighborInfo.Trim())).ToList()[0]);
                                }
                            }
                        }
                        //If a node ID is found that doesnt exist in the list of nodes.
                        else if (!NodeIDs.Contains(int.Parse(neighborInfo.Trim())))
                        {
                            //Add error to list
                            if (!Errors.Any(error => error.LineInfo.Trim() == neighborInfo.Trim()))
                            {
                                Errors.Add(
                                    new Error(neighborInfo, 
                                    Error.EErrorType.UnknowNode));
                            }
                        }
                    }
                }
            }
        }

        public static bool FileContainsErrors()
        {
            return Errors.Count != 0;
        }
    }
}
