﻿using System.Text;

namespace NetworkPlan.FileHelper
{
    public class Error
    {
        public enum EErrorType
        {
            MissingSemiColon,
            MissingNodeInfo,
            UnknowNode,
            EmptyFile,
            MissingHeaderComment
        }

        public EErrorType ErrorType
        { get; private set; }
        public string LineInfo
        { get; private set; }

        public Error(string lineInfo, EErrorType eErrorType)
        {
            ErrorType = eErrorType;
            this.LineInfo = lineInfo;
        }

        public override string ToString()
        {
            StringBuilder stringBuilder = new StringBuilder();

            switch (ErrorType)
            {
                case EErrorType.MissingNodeInfo:
                    stringBuilder
                        .AppendLine("\tDie Zeile: \"" + LineInfo + "\" hat einen Fehler!");
                    stringBuilder
                        .AppendLine("\tEs fehlt Information über den Vorgang!");
                    break;
                case EErrorType.MissingSemiColon:
                    stringBuilder
                        .AppendLine("\tDie Zeile: \"" + LineInfo + "\" hat einen Fehler!");
                    stringBuilder
                        .AppendLine("\tEs fehlt ein Semikolon zur Trennung der Daten!");
                    break;
                case EErrorType.UnknowNode:
                    stringBuilder
                        .AppendLine("\tEs wurde ein Vorgang als Vorgänger/Nachgänger gefunden der nicht existiert!");
                    stringBuilder
                        .AppendLine("\tDer Vorgang: " + LineInfo + " existiert nicht!");
                    break;
                case EErrorType.EmptyFile:
                    stringBuilder
                        .AppendLine("\tEs wurde eine Datei ohne Inhalt gefunden!");
                    break;
                case EErrorType.MissingHeaderComment:
                    stringBuilder
                        .AppendLine("\tDie benötigte Zeile \"//Vorgangsnummer; Vorgangsbezeichnung; Dauer; Vorgänger; Nachfolger\" wurde nicht gefunden!");
                    break;
            }


            return stringBuilder.ToString();
        }
    }
}
