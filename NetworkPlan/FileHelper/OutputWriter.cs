﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace NetworkPlan.FileHelper
{
    public static class OutputWriter
    {
        public static string HeaderComment = "";
        private static FileStream _fileStream;

        private static void Write(string info, bool fileOnly = false)
        {
            if (fileOnly)
            {
                _fileStream.Write(
                    Encoding.UTF8.GetBytes(info), 
                    0, 
                    Encoding.UTF8.GetByteCount(info));
            }
            else
            {
                _fileStream.Write(
                    Encoding.UTF8.GetBytes(info), 
                    0, 
                    Encoding.UTF8.GetByteCount(info));
                Console.WriteLine(info);
            }
        }

        public static void Write(NetworkPlan networkPlan)
        {
            InitFile();
            Write(HeaderComment);
            Write("\r\n", true);
            Write("\r");
            Write("\n", true);
            if (!networkPlan.HasLoops)
            {
                Write(networkPlan.ToString());
            }
            else
            {
                Write("Berechnung nicht möglich.\r\nZyklus erkannt ");
                foreach (Path path in networkPlan.CyclePaths)
                {
                    Write(path.ToString() + "\r\n");
                }
            }
        }

        public static void SetHeaderComment(string headerComment)
        {
            HeaderComment = headerComment;
        }

        private static void InitFile()
        {
            string outputPath = "";
#if DEBUG
            outputPath = @"..\..\..\TestData\Output";
#else
            outputPath = System.IO.Path.Combine(Directory.GetCurrentDirectory(), "Output");
#endif
            if (!Directory.Exists(outputPath))
            {
                Directory.CreateDirectory(outputPath);
            }

            _fileStream = 
                new FileStream(
                    System.IO.Path.Combine(
                        outputPath, 
                        HeaderComment + ".txt"), 
                    FileMode.Create);
        }

        public static void PrintErrors(List<Error> errors)
        {
            if (errors.Count == 0)
            {
                Console.WriteLine("Es wurden keine Fehler in der Input Datei gefunden :D\n");
                return;
            }

            Console.WriteLine("Es wurden Fehler in der Input Datei gefunden :'(\n{\n");
            foreach (Error error in errors)
            {
                Console.WriteLine(error.ToString());
            }
            Console.WriteLine("}");
        }
    }
}