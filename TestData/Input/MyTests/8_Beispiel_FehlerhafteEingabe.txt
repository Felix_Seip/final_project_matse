﻿//***************************************
//+ Beispiel 12 - Fehlerhaft Eingabe
//***************************************
//Vorgangsnummer; Vorgangsbezeichnung; Dauer; Vorgänger; Nachfolger
1; Kaffe mahlen; 1; - 2
2; Kaffe in Trichter füllen; 1; 1; 3
3; Wasser kochen; 4; -; 4,7
4; Kaffe brühen; ; 2,3; 
5; Kaffe trinken; 3; 4; -
6; Tee in Beutel füllen; 1; -; 7,9,0
7; Tee aufsetzen; 5 3,6; 8
8; Tee trinken; 3; 7; -