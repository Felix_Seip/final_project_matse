﻿//***************************************
//+ Beispiel 8 - Loop with multiple start nodes
//***************************************
//Vorgangsnummer; Vorgangsbezeichnung; Dauer; Vorgänger; Nachfolger
1; Kaffe mahlen; 1; -; 2
2; Kaffe in Trichter füllen; 1; 1; 4
3; Wasser kochen; 4; -; 4,7
4; Kaffe brühen; 6; 2,3; 5, 1
5; Kaffe trinken; 3; 4; -
6; Tee in Beutel füllen; 1; -; 7
7; Tee aufsetzen; 5; 3,6; 8
8; Tee trinken; 3; 7; -