@echo off
echo --- start: start.bat ---

echo %1

IF [%1]==[] goto :default
goto :custom

:default
set @TEST_DIR=Input
goto :rest

:custom
set @TEST_DIR=%1
goto :rest

rem loop through all txt-Files
rem TODO: Zwischen Ein- und Ausgabedatei unterscheiden...
:rest
set @FILEWITHPARAMS=NetworkPlan.exe

for /f "delims=" %%a in ('dir /s /b %@TEST_DIR%\*.txt') do %@FILEWITHPARAMS% %%a

echo --- end: start.bat ---
pause
goto :EOF