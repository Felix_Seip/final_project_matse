﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetworkPlan;
using NetworkPlan.FileHelper;

namespace NetworkPlan_Test
{
    [TestClass]
    public class Node_Test
    {
        static List<NetworkPlan.NetworkPlan> networkPlans;
        const string TEST_DIR = @"..\..\..\TestData\Input";

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            networkPlans = new List<NetworkPlan.NetworkPlan>();
            foreach (string file in Directory.GetFiles(TEST_DIR, "*.txt", SearchOption.AllDirectories))
            {
                List<Node> startNodes = InputReader.Analyze(InputReader.ReadFile(file));
                if(startNodes == null)
                {
                    continue;
                }
                NetworkPlan.NetworkPlan networkPlan = new NetworkPlan.NetworkPlan(startNodes, InputReader.AllNodes);
                networkPlan.FindCriticalPaths();
                networkPlans.Add(networkPlan);
            }
        }

        [TestMethod]
        public void GetAllParents_Test()
        {
            bool containsCycle = networkPlans[3].AllNodes[3].Children.Any(x => Node.GetAllParentNodes(networkPlans[3].AllNodes[3]).Any(y => x.Equals(y)));
            Assert.IsTrue(containsCycle);
        }
    }
}
