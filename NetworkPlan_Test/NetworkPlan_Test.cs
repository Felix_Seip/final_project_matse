﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetworkPlan;
using NetworkPlan.FileHelper;
using System.Collections.Generic;
using System.IO;

namespace NetworkPlan_Test
{
    [TestClass]
    public class NetworkPlan_Test
    {
        static List<NetworkPlan.NetworkPlan> networkPlans;
        const string TEST_DIR = @"..\..\..\TestData\Input";

        [ClassInitialize]
        public static void Init(TestContext testContext)
        {
            networkPlans = new List<NetworkPlan.NetworkPlan>();
            foreach (string file in Directory.GetFiles(TEST_DIR, "*.txt", SearchOption.AllDirectories))
            {
                List<Node> startNodes = InputReader.Analyze(InputReader.ReadFile(file));
                if(startNodes == null)
                {
                    continue;
                }

                NetworkPlan.NetworkPlan networkPlan = new NetworkPlan.NetworkPlan(startNodes, InputReader.AllNodes);
                networkPlan.FindCriticalPaths();
                networkPlans.Add(networkPlan);
                OutputWriter.Write(networkPlan);
            }
        }

        [TestMethod]
        public void NetworkPlanDuration_Test()
        {
            double[] expected = new double[] { 39, 28, -1, -1, 23, -1, -1, -1, 1, -1, 66 };
            for (int i = 0; i < networkPlans.Count; i++)
            {
                Assert.AreEqual(expected[i], networkPlans[i].ProjectDuration);
            }
        }

        [TestMethod]
        public void NetworkCycle_Test()
        {
            int index = 0;
            foreach (NetworkPlan.NetworkPlan networkPlan in networkPlans)
            {
                if (index == 3 || index == 5 || index == 6 || index == 7 || index == 9)
                {
                    Assert.AreEqual(networkPlan.HasLoops, true);
                }
                else
                {
                    Assert.AreEqual(networkPlan.HasLoops, false);
                }
                index++;
            }
        }

        [TestMethod]
        public void NetworkCyclePath_Test()
        {
            string[] cyclePaths = new string[]
            {
                "3->4->3",
                "1->2->3->4->5->1",
                "1->2->3->4->5->1", 
                "3->4->5->3",
                "2->3->4->5->2",
                "1->2->4->1",
                "1->1"
            };

            int index = 0;
            int cycleIndex = 0;
            foreach (NetworkPlan.NetworkPlan networkPlan in networkPlans)
            {
                if (index == 3 || index == 5 || index == 6 || index == 7 || index == 9)
                {
                    foreach(NetworkPlan.Path path in networkPlan.CyclePaths)
                    {
                        Assert.AreEqual(path.ToString(), cyclePaths[cycleIndex]);
                        cycleIndex++;
                    }
                }
                else
                {
                    Assert.AreEqual(networkPlan.CyclePaths.Count, 0);
                }
                index++;
            }
        }

        [TestMethod]
        public void NetworkFullyConnected_Test()
        {
            foreach(NetworkPlan.NetworkPlan networkPlan in networkPlans)
            {
                Assert.IsTrue(networkPlan.IsFullyConnected());
            }
        }

        [TestMethod]
        public void NetworkCriticalPath_Test()
        {
            string[] expectedPathOutputs = new string[]
            {
                "1->2->3->6->7",
                "1->2->3->4->5->6",
                "3->4->5",
                "3->7->8",
                "1->2->5->7->11->13->14->17",
                "1",
                "1->4->5->7->8"
            };

            int expectedIndex = 0;
            for (int i = 0; i < networkPlans.Count; i++)
            {
                for (int j = 0; j < networkPlans[i].CriticalPaths.Count; j++)
                {
                    Assert.AreEqual(expectedPathOutputs[expectedIndex], networkPlans[i].CriticalPaths[j].ToString());
                    expectedIndex++;
                }
            }
        }
    }
}
