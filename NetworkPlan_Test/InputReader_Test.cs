﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using NetworkPlan;
using NetworkPlan.FileHelper;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace NetworkPlan_Test
{
    [TestClass]
    public class InputReader_Test
    {
        const string TEST_DIR = @"..\..\..\TestData\Input";

        [TestMethod]
        public void StartNodeCount_Test()
        {
            int[] expectedStartNodeCount = new int[] { 1, 1, 3, 1, 1, 1, 1, 3, 1, 1, 1 };

            int index = 0;
            foreach (string file in Directory.GetFiles(TEST_DIR, "*.txt", SearchOption.AllDirectories))
            {
                List<Node> startNodeList = InputReader.Analyze(InputReader.ReadFile(file));
                if (InputReader.FileContainsErrors())
                {
                    continue;
                }

                Assert.AreEqual(expectedStartNodeCount[index], startNodeList.Count);
                index++;
            }
        }

        [TestMethod]
        public void EndNodeCount_Test()
        {
            int[] expectedStartNodeCount = new int[] { 1, 1, 2, 1, 1, 1, 1, 2, 1, 0, 1 };

            int index = 0;
            foreach (string file in Directory.GetFiles(TEST_DIR, "*.txt", SearchOption.AllDirectories))
            {
                InputReader.Analyze(InputReader.ReadFile(file));
                if (InputReader.FileContainsErrors())
                {
                    continue;
                }

                Assert.AreEqual(expectedStartNodeCount[index], InputReader.AllNodes.Where(node => node.Children.Count == 0).ToList().Count);
                index++;
            }
        }

        [TestMethod]
        public void NodeCount_Test()
        {
            int[] expectedStartNodeCount = new int[] { 7, 6, 8, 6, 17, 6, 6, 8, 1, 1, 8 };

            int index = 0;
            foreach (string file in Directory.GetFiles(TEST_DIR, "*.txt", SearchOption.AllDirectories))
            {
                InputReader.Analyze(InputReader.ReadFile(file));
                if (InputReader.FileContainsErrors())
                {
                    continue;
                }

                Assert.AreEqual(expectedStartNodeCount[index], InputReader.AllNodes.Count);
                index++;
            }
        }

        [TestMethod]
        public void HeaderComment_Test()
        {
            string[] expectedStartNodeCount = new string[]
            {
                "Beispiel 1 - Installation von POI Kiosken",
                "Beispiel 2 - Wasserfallmodell",
                "Beispiel 3",
                "Beispiel 4 mit Zyklus",
                "Beispiel 5 IT-Installation",
                "Beispiel 6 mit Big Zyklus",
                "Beispiel 7 mit Multi Zyklus",
                "Beispiel 8 - Loop with multiple start nodes",
                "Beispiel 9 - Minimal Beispiel",
                "Beispiel 10 - Minimal Loop",
                "Beispiel 11 aus dem Internet Geklaut",
                "Beispiel 12 - Fehlerhaft Eingabe"
            };

            int index = 0;
            foreach (string file in Directory.GetFiles(TEST_DIR, "*.txt", SearchOption.AllDirectories))
            {
                InputReader.Analyze(InputReader.ReadFile(file));
                Assert.AreEqual(expectedStartNodeCount[index], InputReader.HeaderComment);
                index++;
            }
        }

        [TestMethod]
        public void IsValidInputFile_Test()
        {
            InputReader.Analyze(InputReader.ReadFile(TEST_DIR + "\\MyTests\\8_Beispiel_FehlerhafteEingabe.txt"));
            Assert.IsTrue(InputReader.FileContainsErrors());
        }
    }
}
